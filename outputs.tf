output "fargate_service" {
  value = aws_ecs_service.this
}

output "ecs_cluster" {
  value = local.create_new_cluster ? aws_ecs_cluster.new[0] : data.aws_ecs_cluster.existing[0]
}

output "fargate_service_security_group" {
  value = aws_security_group.this
}

output "task_definition" {
  value = aws_ecs_task_definition.this
}

output "cloudwatch_log_group" {
  value = aws_cloudwatch_log_group.this
}

output "autoscaling_step_up_policy" {
  value = var.autoscaling_config != null ? aws_appautoscaling_policy.up : null
}

output "autoscaling_step_down_policy" {
  value = var.autoscaling_config != null ? aws_appautoscaling_policy.down : null
}

output "task_role" {
  value = aws_iam_role.task
}

output "task_execution_role" {
  value = aws_iam_role.task_execution
}
