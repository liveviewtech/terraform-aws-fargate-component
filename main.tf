terraform {
  required_version = "~>1"
  required_providers {
    aws = ">=3.50"
  }
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

locals {
  name               = coalesce(var.name, var.app_name)
  app_name           = var.name_prefix != null ? "${var.name_prefix}-${local.name}" : local.name
  create_new_cluster = var.ecs_cluster_name == null
  cluster_name       = local.create_new_cluster ? local.app_name : var.ecs_cluster_name
  definitions        = concat([var.primary_container_definition], var.extra_container_definitions)
  volumes = distinct(flatten([
    for def in local.definitions :
    try(def.efs_volume_mounts, null) != null ? def.efs_volume_mounts : []
  ]))
  ssm_parameters = distinct(flatten([
    for def in local.definitions :
    values(def.secrets != null ? def.secrets : {})
  ]))
  has_secrets            = length(local.ssm_parameters) > 0
  ssm_parameter_arn_base = "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/"
  secrets_arns = [
    for param in local.ssm_parameters :
    "${local.ssm_parameter_arn_base}${replace(param, "/^//", "")}"
  ]
  cloudwatch_log_group_name = var.name_prefix != null ? "/${var.name_prefix}/${local.name}" : "/${local.name}"

  container_definitions = [
    for def in local.definitions : {
      name         = def.name
      image        = def.image
      essential    = try(def.essential, true)
      privileged   = false
      portMappings = []
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = local.cloudwatch_log_group_name
          awslogs-region        = data.aws_region.current.name
          awslogs-stream-prefix = local.name
        }
      }
      environment = [
        for key in keys(def.environment_variables != null ? def.environment_variables : {}) :
        {
          name  = key
          value = lookup(def.environment_variables, key)
        }
      ]
      secrets = [
        for key in keys(def.secrets != null ? def.secrets : {}) :
        {
          name      = key
          valueFrom = "${local.ssm_parameter_arn_base}${replace(lookup(def.secrets, key), "/^//", "")}"
        }
      ]
      mountPoints = [
        for mount in(try(def.efs_volume_mounts, null) != null ? def.efs_volume_mounts : []) :
        {
          containerPath = mount.container_path
          sourceVolume  = mount.name
          readOnly      = false
        }
      ]
      volumesFrom = []
      command     = try(def.command, null)
    }
  ]
}

# == Task Definition == #
data "aws_iam_policy_document" "task_execution" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "task_execution" {
  name                 = "${local.app_name}_task-execution"
  assume_role_policy   = data.aws_iam_policy_document.task_execution.json
  permissions_boundary = var.role_permissions_boundary_arn
  tags                 = var.tags
}

resource "aws_iam_role_policy_attachment" "task_execution" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.task_execution.name
}

data "aws_iam_policy_document" "secrets_access" {
  count   = local.has_secrets ? 1 : 0
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "ssm:GetParameters",
      "ssm:GetParameter",
      "ssm:GetParemetersByPath"
    ]
    resources = local.secrets_arns
  }
}

resource "aws_iam_policy" "secrets_access" {
  count  = local.has_secrets ? 1 : 0
  name   = "${local.app_name}_secrets-access"
  policy = data.aws_iam_policy_document.secrets_access[0].json
}

resource "aws_iam_role_policy_attachment" "secrets_policy" {
  count      = local.has_secrets ? 1 : 0
  policy_arn = aws_iam_policy.secrets_access[0].arn
  role       = aws_iam_role.task_execution.name
}

data "aws_iam_policy_document" "task_policy" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "task" {
  name                 = "${local.app_name}_task"
  assume_role_policy   = data.aws_iam_policy_document.task_policy.json
  permissions_boundary = var.role_permissions_boundary_arn
  tags                 = var.tags
}

resource "aws_iam_role_policy_attachment" "task" {
  count      = length(var.task_policies)
  policy_arn = element(var.task_policies, count.index)
  role       = aws_iam_role.task.name
}

resource "aws_iam_role_policy_attachment" "task_secrets_access" {
  count      = local.has_secrets ? 1 : 0
  policy_arn = aws_iam_policy.secrets_access[0].arn
  role       = aws_iam_role.task.name
}

resource "aws_ecs_task_definition" "this" {
  container_definitions    = jsonencode(local.container_definitions)
  family                   = local.app_name
  cpu                      = var.task_cpu
  memory                   = var.task_memory
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.task_execution.arn
  task_role_arn            = aws_iam_role.task.arn
  tags                     = var.tags

  dynamic "volume" {
    for_each = local.volumes
    content {
      name = volume.value.name
      efs_volume_configuration {
        file_system_id = volume.value.file_system_id
        root_directory = volume.value.root_directory
      }
    }
  }
}

# == Fargate == #
resource "aws_ecs_cluster" "new" {
  count = local.create_new_cluster ? 1 : 0 # if custer is not provided create one
  name  = local.cluster_name
  tags  = var.tags

  setting {
    name  = "containerInsights"
    value = var.enable_container_insights ? "enabled" : "disabled"
  }
}

data "aws_ecs_cluster" "existing" {
  count        = local.create_new_cluster ? 0 : 1
  cluster_name = var.ecs_cluster_name
}

resource "aws_security_group" "this" {
  name   = "${local.app_name}_fargate"
  vpc_id = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = var.tags
}

resource "aws_ecs_service" "this" {
  name             = local.name
  task_definition  = aws_ecs_task_definition.this.arn
  cluster          = local.create_new_cluster ? aws_ecs_cluster.new[0].id : data.aws_ecs_cluster.existing[0].id # if cluster is not provided use created one, else use existing cluster
  desired_count    = var.autoscaling_config != null ? var.autoscaling_config.min_capacity : 1
  launch_type      = "FARGATE"
  platform_version = var.fargate_platform_version

  force_new_deployment  = var.force_new_deployment
  wait_for_steady_state = var.wait_for_steady_state

  deployment_controller {
    type = "ECS"
  }

  network_configuration {
    subnets          = var.private_subnet_ids
    security_groups  = concat([aws_security_group.this.id], var.security_groups)
    assign_public_ip = false
  }

  tags = var.tags

  lifecycle {
    ignore_changes = [desired_count]
  }
}

# == CloudWatch == #
resource "aws_cloudwatch_log_group" "this" {
  name              = local.cloudwatch_log_group_name
  retention_in_days = var.log_retention_in_days
  tags              = var.tags
}

# == AutoScaling == #
resource "aws_appautoscaling_target" "this" {
  count              = var.autoscaling_config != null ? 1 : 0
  min_capacity       = var.autoscaling_config.min_capacity
  max_capacity       = var.autoscaling_config.max_capacity
  resource_id        = "service/${local.cluster_name}/${aws_ecs_service.this.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "up" {
  count              = var.autoscaling_config != null ? 1 : 0
  name               = "${local.app_name}_autoscale-up"
  resource_id        = aws_appautoscaling_target.this[0].resource_id
  scalable_dimension = aws_appautoscaling_target.this[0].scalable_dimension
  service_namespace  = aws_appautoscaling_target.this[0].service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    metric_aggregation_type = "Average"
    cooldown                = 300

    step_adjustment {
      scaling_adjustment          = 1
      metric_interval_lower_bound = 0
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "up" {
  count      = var.autoscaling_config != null ? 1 : 0
  alarm_name = "${local.app_name}_alarm-up"
  namespace  = "AWS/ECS"
  dimensions = {
    ClusterName = local.cluster_name
    ServiceName = aws_ecs_service.this.name
  }
  statistic           = "Average"
  metric_name         = "CPUUtilization"
  comparison_operator = "GreaterThanThreshold"
  threshold           = 75
  period              = 300
  evaluation_periods  = 5
  alarm_actions       = [aws_appautoscaling_policy.up[0].arn]
  tags                = var.tags
}

resource "aws_appautoscaling_policy" "down" {
  count              = var.autoscaling_config != null ? 1 : 0
  name               = "${local.name}_autoscale-down"
  resource_id        = aws_appautoscaling_target.this[0].resource_id
  scalable_dimension = aws_appautoscaling_target.this[0].scalable_dimension
  service_namespace  = aws_appautoscaling_target.this[0].service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    metric_aggregation_type = "Average"
    cooldown                = 300

    step_adjustment {
      scaling_adjustment          = -1
      metric_interval_upper_bound = 0
    }
  }
}

resource "aws_cloudwatch_metric_alarm" "down" {
  count      = var.autoscaling_config != null ? 1 : 0
  alarm_name = "${local.app_name}_alarm-down"
  namespace  = "AWS/ECS"
  dimensions = {
    ClusterName = local.cluster_name
    ServiceName = aws_ecs_service.this.name
  }
  statistic           = "Average"
  metric_name         = "CPUUtilization"
  comparison_operator = "LessThanThreshold"
  threshold           = 25
  period              = 300
  evaluation_periods  = 5
  alarm_actions       = [aws_appautoscaling_policy.down[0].arn]
  tags                = var.tags
}
