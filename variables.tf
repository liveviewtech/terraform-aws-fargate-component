variable "name_prefix" {
  type    = string
  default = null
}

variable "name" {
  type        = string
  default     = null # TODO: remove this after app_name is removed
  description = "Application name to name your Fargate Component and other resources"
}

variable "app_name" {
  type        = string
  default     = null
  description = "DEPRECATED: please use the 'name' input variable instead"
}

variable "ecs_cluster_name" {
  type        = string
  default     = null
  description = "ECS Cluster name to host the fargate server. Defaults to creating its own cluster."
}

variable "enable_container_insights" {
  type        = bool
  default     = true
  description = "Enables the capture of ECS Container Insights metrics."
}

variable "primary_container_definition" {
  type        = any
  description = "The primary container definition for your application.'"
}

variable "extra_container_definitions" {
  type        = any
  default     = []
  description = "A list of extra container definitions. Defaults to []"
}

variable "task_policies" {
  type        = list(string)
  default     = []
  description = "List of IAM Policy ARNs to attach to the task execution policy."
}

variable "task_cpu" {
  type        = number
  default     = 256
  description = "CPU for the task definition. Defaults to 256."
}

variable "task_memory" {
  type        = number
  default     = 512
  description = "Memory for the task definition. Defaults to 512."
}

variable "security_groups" {
  type        = list(string)
  default     = []
  description = "List of extra security group IDs to attach to the fargate task."
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to deploy ECS fargate service."
}

variable "private_subnet_ids" {
  type        = list(string)
  description = "List of subnet IDs for the fargate service."
}

variable "role_permissions_boundary_arn" {
  type        = string
  description = "ARN of the IAM Role permissions boundary to place on each IAM role created."
}

variable "autoscaling_config" {
  type = object({
    min_capacity = number
    max_capacity = number
  })
  default     = null
  description = "Configuration for default autoscaling policies and alarms. Set to null if you want to set up your own autoscaling policies and alarms."
}

variable "log_retention_in_days" {
  type        = number
  default     = 120
  description = "CloudWatch log group retention in days. Defaults to 120."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "A map of AWS Tags to attach to each resource created"
}

variable "fargate_platform_version" {
  type        = string
  default     = "1.4.0"
  description = "Version of the Fargate platform to run."
}

variable "force_new_deployment" {
  type    = bool
  default = true
}

variable "wait_for_steady_state" {
  type        = bool
  default     = true
  description = "Enable to force a new task deployment of the service"
}
