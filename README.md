# Terraform AWS Fargate Component

Terraform module pattern to build a standard Fargate.

This module creates a Fargate service that cannot be routed to through HTTP/S.

**Note:** This module will not be able to host any web servers or APIs. If you require this please use this [Module](https://bitbucket.org/liveviewtech/terraform-aws-fargate-api).

## Usage

```hcl
module "component" {
  source = "bitbucket.org/liveviewtech/terraform-aws-fargate-component.git?ref=v2.2.0"
  name = "example-component"
  primary_container_definition = {
    name  = "example"
    image = "lvt/example-component"
    environment_variables = {
      LOG = "debug"
    }
    secrets = {
      SUPER_SECRET = aws_ssm_parameter.super_secret.name
    }
  }

  private_subnet_ids            = module.acs.private_subnet_ids
  vpc_id                        = module.acs.vpc.id
  role_permissions_boundary_arn = module.acs.role_permissions_boundary.arn
}
```

## Created Resources

- ECS Cluster (if not provided)
- ECS Service
  - with security group
- ECS Task Definition
  - with IAM role
- CloudWatch Log Group
- AutoScaling Target
- AutoScaling Policies (one for stepping up and one for stepping down)
- CloudWatch Metric Alarms (one for stepping up and one for stepping down)

## Requirements

- Terraform version 1.0.0 or greater

## Inputs

| Name                          | Type                                  | Description                                                                                                                                                                                              | Default |
| ----------------------------- | ------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| name_prefix                   | string                                | optional string prefix to use on various resources to prevent collisions                                                                                                                                 | null    |
| name                          | string                                | Application name to name your Fargate API and other resources                                                                                                                                            |         |
| ecs_cluster_name              | string                                | Existing ECS Cluster name to host the fargate server. Defaults to creating its own cluster                                                                                                               | <name>  |
| enable_container_insights     | bool                                  | Enables the capture of ECS Container Insights metrics.                                                                                                                                                   | true    |
| primary_container_definition  | [object](#container_definition)       | The primary container definition for your application. This one will be the only container that receives traffic from the ALB, so make sure the `ports` field contains the same port as the `image_port` |         |
| extra_container_definitions   | list([object](#container_definition)) | A list of extra container definitions (side car containers)                                                                                                                                              | []      |
| task_policies                 | list(string)                          | List of IAM Policy ARNs to attach to the task execution IAM Policy                                                                                                                                       | []      |
| task_cpu                      | number                                | CPU for the task definition                                                                                                                                                                              | 256     |
| task_memory                   | number                                | Memory for the task definition                                                                                                                                                                           | 512     |
| security_groups               | list(string)                          | List of extra security group IDs to attach to the fargate task                                                                                                                                           | []      |
| vpc_id                        | string                                | VPC ID to deploy the ECS fargate service and ALB                                                                                                                                                         |         |
| private_subnet_ids            | list(string)                          | List of subnet IDs for the fargate service                                                                                                                                                               |         |
| role_permissions_boundary_arn | string                                | ARN of the IAM Role permissions boundary to place on each IAM role created                                                                                                                               |         |
| autoscaling_config            | [object](#autoscaling_config)         | Configuration for default autoscaling policies and alarms. Set to `null` if you want to set up your own autoscaling policies and alarms                                                                  |         |
| log_retention_in_days         | number                                | CloudWatch log group retention in days                                                                                                                                                                   | 120     |
| tags                          | map(string)                           | A map of AWS Tags to attach to each resource created                                                                                                                                                     | {}      |
| fargate_platform_version      | string                                | Version of the Fargate platform to run                                                                                                                                                                   | 1.4.0   |
| force_new_deployment          | bool                                  | Enable to force a new task deployment of the service. This can be used to update tasks to use a newer Docker image with same image/tag combination                                                       | true    |
| wait_for_steady_state         | bool                                  | If true, Terraform will wait for the service to reach a steady state before continuing. The deployment will fail if the service does not become stable after 10 minutes                                  | true    |

#### container_definition

Object with following attributes to define the docker container(s) your fargate needs to run.

- **`name`** - (Required) container name (referenced in CloudWatch logs, and possibly by other containers)
- **`image`** - (Required) the ecr_image_url with the tag like: `<acct_num>.dkr.ecr.us-west-2.amazonaws.com/myapp:dev` or the image URL from dockerHub or some other docker registry
- **`environment_variables`** - (Required) a map of environment variables to pass to the docker container
- **`secrets`** - (Required) a map of secrets from the parameter store to be assigned to env variables
- **`efs_volume_mounts`** - (Required) a list of efs_volume_mount [objects](#efs_volume_mount) to be mounted into the container.

**Before running this configuration** make sure that your ECR repo exists and an image has been pushed to the repo.

#### efs_volume_mount

Example

```
    efs_volume_mounts = [
      {
        name = "persistent_data"
        file_system_id = aws_efs_file_system.my_efs.id
        root_directory = "/"
        container_path = "/usr/app/data"
      }
    ]
```

- **`name`** - A logical name used to describe what the mount is for.
- **`file_system_id`** - ID of the EFS to mount.
- **`root_directory`** - Source path inside the EFS.
- **`container_path`** - Target path inside the container.

See the following docs for more details:

- https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html#volume-block-arguments
- https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html#efs-volume-configuration-arguments
- https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ContainerDefinition.html
- https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_MountPoint.html

#### autoscaling_config

This module will create basic default autoscaling policies and alarms and you can define some variables of these default autoscaling policies.

- **`min_capacity`** - (Required) Minimum task count for autoscaling (this will also be used to define the initial desired count of the ECS Fargate Service)
- **`max_capacity`** - (Required) Maximum task count for autoscaling

**Note:** If you want to define your own autoscaling policies/alarms then you need to set this field to `null` at which point this module will not create any policies/alarms.

**Note:** the desired count of the ECS Fargate Service will be set the first time terraform runs but changes to desired count will be ignored after the first time.

#### CloudWatch logs

This module will create a CloudWatch log group named `/fargate/<name>` with log streams named `<name>/<container_name>/<container_id>`.

For instance with the [above example](#usage) the logs could be found in the CloudWatch log group: `/fargate/example-api` with the container logs in `example-api/example/12d344fd34b556ae4326...`

## Outputs

| Name                           | Type                                                                                                                | Description                                                      |
| ------------------------------ | ------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- |
| fargate_service                | [object](https://www.terraform.io/docs/providers/aws/r/ecs_service.html#attributes-reference)                       | Fargate ECS Service object                                       |
| ecs_cluster                    | [object](https://www.terraform.io/docs/providers/aws/r/ecs_cluster.html#attributes-reference)                       | ECS Cluster (created or pre-existing) the service is deployed on |
| fargate_service_security_group | [object](https://www.terraform.io/docs/providers/aws/r/security_group.html#attributes-reference)                    | Security Group object assigned to the Fargate service            |
| task_definition                | [object](https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html#attributes-reference)               | The task definition object of the fargate service                |
| autoscaling_step_up_policy     | [object](https://www.terraform.io/docs/providers/aws/r/autoscaling_policy.html#attributes-reference)                | Autoscaling policy to step up                                    |
| autoscaling_step_down_policy   | [object](https://www.terraform.io/docs/providers/aws/r/autoscaling_policy.html#attributes-reference)                | Autoscaling policy to step down                                  |
| task_role                      | [object](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role#attributes-reference) | IAM role created for the tasks.                                  |
| task_execution_role            | [object](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role#attributes-reference) | IAM role created for the execution of tasks.                     |
